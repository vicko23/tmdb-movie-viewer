export const environment = {
  production: true,
  baseApiUrl: 'https://api.themoviedb.org/3/',
  apiKey: '4ff9d08260ed338797caa272d7df35dd',
  movieImagesBasePath: 'https://image.tmdb.org/t/p/w500/'
};
