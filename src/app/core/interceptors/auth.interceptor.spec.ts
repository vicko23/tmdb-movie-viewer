import { TestBed } from '@angular/core/testing';
import {
  HttpClientTestingModule,
  HttpTestingController,
} from '@angular/common/http/testing';
import { MoviesService } from '../services/movies.service';
import { AuthInterceptor } from './auth.interceptor';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { environment } from 'src/environments/environment';

describe(`AuthHttpInterceptor`, () => {
  let service: MoviesService;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        MoviesService,
        {
          provide: HTTP_INTERCEPTORS,
          useClass: AuthInterceptor,
          multi: true,
        },
      ],
    });

    service = TestBed.inject(MoviesService);
    httpMock = TestBed.inject(HttpTestingController);
  });

  it('should add an key to url', () => {
    service.getMovies().subscribe(response => {
      expect(response).toBeTruthy();
    });

    const apiKey = environment.apiKey;
    const httpRequest = httpMock.expectOne(environment.baseApiUrl + 'discover/movie?sort_by=popularity.desc&api_key=' + apiKey);

    expect(httpRequest.request.url).toContain(apiKey);
  });

});
