import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
    private clonedRequest: HttpRequest<any>;

    constructor() { }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        // add api key to request url
        const newUrl = request.url + (request.url.includes('?') ? '&api_key=' : '?api_key=');
        this.clonedRequest = request.clone({ url: newUrl + environment.apiKey });

        return next.handle(this.clonedRequest ? this.clonedRequest : request);
    }
}
