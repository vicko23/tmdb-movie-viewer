import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment';
import { Movie } from '../../shared/models/movie';
import { map } from 'rxjs/operators';

@Injectable()
export class MoviesService {
  constructor(private http: HttpClient) { }

 /**
 * Get movies
 * maps data from API to Movie model
 * @returns observable of Movie array
 */
  getMovies(): Observable<Movie[]> {
    return this.http
      .get<any>(environment.baseApiUrl + 'discover/movie?sort_by=popularity.desc')
      .pipe(
        map((resp) => {
          const movies: Movie[] = [];
          if (resp && resp.results && resp.results.length) {
            resp.results.forEach((respMovie: any) => {
              movies.push({
                Id: respMovie.id,
                Adult: respMovie.adult,
                BackdropPath: respMovie.backdrop_path,
                OriginalLanguage: respMovie.original_language,
                OriginalTitle: respMovie.original_title,
                Overview: respMovie.overview,
                Popularity: respMovie.popularity,
                PosterPath: respMovie.poster_path,
                ReleaseDate: respMovie.release_date,
                Title: respMovie.title,
                VoteAverage: respMovie.vote_average,
                VoteCount: respMovie.vote_count,
              });
            });
          }
          return movies;
        })
      );
  }
}
