import { ErrorHandler, Injectable } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';

@Injectable()
export class GlobalErrorHandler implements ErrorHandler {
    constructor() { }

    handleError(error: any): void {
        if (error instanceof HttpErrorResponse) {
            // console and show when any error happens
            console.log(error);
            alert(error);
            // Log error here
            throw error;
        }
    }
}
