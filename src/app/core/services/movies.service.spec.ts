import { MoviesService } from './movies.service';
import { Movie } from 'src/app/shared/models/movie';
import { of } from 'rxjs';

const moviesResp = {
    results: [
        {
            popularity: 809213,
            vote_count: 898,
            poster_path: '/TnOeov4w0sTtV2gqICqIxVi74V.jpg',
            id: 605116,
            adult: false,
            backdrop_path: '/qVygtf2vU15L2yKS4Ke44U4oMdD.jpg',
            original_language: 'en',
            original_title: 'Project Power',
            title: 'Project Power',
            vote_average: 6.7,
            overview: 'An ex-soldier, a teen and a cop collide in New Orleans as they hunt for the source behind a dangerous new pill that grants users temporary superpowers.',
            release_date: '2020-08-14'
        },
        {
            popularity: 610345,
            vote_count: 91,
            poster_path: '/uOw5JD8IlD546feZ6oxbIjvN66P.jpg',
            id: 718444,
            adult: false,
            backdrop_path: '/x4UkhIQuHIJyeeOTdcbZ3t3gBSa.jpg',
            original_language: 'en',
            original_title: 'Rogue',
            title: 'Rogue',
            vote_average: 5.9,
            overview: 'Battle-hardened O’Hara leads a lively mercenary team of soldiers on a daring mission: rescue hostages from their captors in remote Africa. But as the mission goes awry and the team is stranded, O’Hara’s squad must face a bloody, brutal encounter with a gang of rebels.',
            release_date: '2020-08-20'
        }
    ]
};
const moviesMapped: Movie[] = [
    {
        Popularity: 809213,
        VoteCount: 898,
        PosterPath: '/TnOeov4w0sTtV2gqICqIxVi74V.jpg',
        Id: 605116,
        Adult: false,
        BackdropPath: '/qVygtf2vU15L2yKS4Ke44U4oMdD.jpg',
        OriginalLanguage: 'en',
        OriginalTitle: 'Project Power',
        Title: 'Project Power',
        VoteAverage: 6.7,
        Overview: 'An ex-soldier, a teen and a cop collide in New Orleans as they hunt for the source behind a dangerous new pill that grants users temporary superpowers.',
        ReleaseDate: '2020-08-14'
    },
    {
        Popularity: 610345,
        VoteCount: 91,
        PosterPath: '/uOw5JD8IlD546feZ6oxbIjvN66P.jpg',
        Id: 718444,
        Adult: false,
        BackdropPath: '/x4UkhIQuHIJyeeOTdcbZ3t3gBSa.jpg',
        OriginalLanguage: 'en',
        OriginalTitle: 'Rogue',
        Title: 'Rogue',
        VoteAverage: 5.9,
        Overview: 'Battle-hardened O’Hara leads a lively mercenary team of soldiers on a daring mission: rescue hostages from their captors in remote Africa. But as the mission goes awry and the team is stranded, O’Hara’s squad must face a bloody, brutal encounter with a gang of rebels.',
        ReleaseDate: '2020-08-20'
    }
];

describe('MoviesService', () => {
    let httpClientSpy: { get: jasmine.Spy };
    let movieService: MoviesService;

    beforeEach(() => {
        httpClientSpy = jasmine.createSpyObj('HttpClient', ['get']);
        movieService = new MoviesService(httpClientSpy as any);
    });

    it('should return expected movies (test mapping)', () => {
        httpClientSpy.get.and.returnValue(of(moviesResp));

        movieService.getMovies().subscribe(
            movies => expect(movies).toEqual(moviesMapped)
        );
    });
});
