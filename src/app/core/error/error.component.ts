import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { HttpErrorResponse } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-error',
  templateUrl: './error.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ErrorComponent implements OnInit {
  errorState$: Observable<HttpErrorResponse | Error>;

  constructor(public activatedRoute: ActivatedRoute) { }

  ngOnInit(): void {
    this.errorState$ = this.activatedRoute.paramMap
      .pipe(map(() => (window.history.state && window.history.state.error) ? JSON.parse(window.history.state.error) : null));
  }

}
