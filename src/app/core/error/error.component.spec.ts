import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ErrorComponent } from './error.component';
import { RouterModule } from '@angular/router';
import { of } from 'rxjs';
import { ChangeDetectionStrategy } from '@angular/core';
import { By } from '@angular/platform-browser';

describe('ErrorComponent', () => {
  let component: ErrorComponent;
  let fixture: ComponentFixture<ErrorComponent>;
  const errorMsg = 'test error';

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ErrorComponent],
      imports: [RouterModule.forRoot([])],
    })
    .overrideComponent(ErrorComponent, {
      set: { changeDetection: ChangeDetectionStrategy.Default }
    })
    .compileComponents();
  }));

  beforeEach(async(() => {
    fixture = TestBed.createComponent(ErrorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should set error', async(() => {
    component.errorState$ = of(new Error(errorMsg));
    component.errorState$.subscribe(error => {
      expect(error.message).toBe(errorMsg);
    });
  }));

  it('should set error paragraph value', async(() => {
    component.errorState$ = of(new Error(errorMsg));
    fixture.detectChanges();
    const errorParagraph = fixture.debugElement.query(By.css('p')).nativeElement;
    expect(errorParagraph.innerText).toBe(errorMsg);
  }));
});
