import { Component, ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'app-popular',
  templateUrl: './popular.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PopularComponent {
  searchTerm: string;
}
