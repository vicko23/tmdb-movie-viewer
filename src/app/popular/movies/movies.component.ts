import { Component, OnInit, Input, ChangeDetectionStrategy } from '@angular/core';
import { Observable } from 'rxjs';
import { Movie } from 'src/app/shared/models/movie';
import { Store } from '@ngrx/store';
import { MoviesState } from 'src/app/store/movies-store/movies.reducer';
import * as moviesSelectors from '../../store/movies-store/movies.selectors';
import * as moviesActions from '../../store/movies-store/movies.actions';
import { Utils } from '../../shared/utils/utils';

@Component({
  selector: 'app-movies',
  templateUrl: './movies.component.html',
  styleUrls: ['./movies.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MoviesComponent implements OnInit {
  @Input() searchTerm: string;

  Utils = Utils;

  movies$: Observable<Movie[]>;
  selectedMovie$: Observable<Movie>;
  moviesLoading$: Observable<boolean>;

  constructor(private store: Store<MoviesState>) { }

  ngOnInit(): void {
    // set observables from store
    this.movies$ = this.store.select(moviesSelectors.selectMovies);
    this.selectedMovie$ = this.store.select(moviesSelectors.selectMovie);
    this.moviesLoading$ = this.store.select(moviesSelectors.selectMoviesLoading);

    // get movies
    this.store.dispatch(moviesActions.loadMovies());
  }

  /**
  * Select movie (on click)
  * @param movie movie to select
  */
  selectMovie(movie: Movie): void {
    this.store.dispatch(moviesActions.selectMovie({ movie }));
  }

}
