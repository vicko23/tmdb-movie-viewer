import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MoviesComponent } from './movies.component';
import { provideMockStore } from '@ngrx/store/testing';

import { PopularModule } from '../popular.module';
import { SharedModule } from '../../shared/shared.module';
import { Movie } from 'src/app/shared/models/movie';
import { of } from 'rxjs';
import { ChangeDetectionStrategy } from '@angular/core';
import { By } from '@angular/platform-browser';

describe('MoviesComponent', () => {
  let component: MoviesComponent;
  let fixture: ComponentFixture<MoviesComponent>;

  const initialState = {
    movies: {
      ids: [],
      entities: [],
      selectedMovie: null,
      statusState: 'INIT'
    }
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [MoviesComponent],
      providers: [
        provideMockStore({ initialState })
      ],
      imports: [
        SharedModule,
        PopularModule
      ]
    })
      .overrideComponent(MoviesComponent, {
        set: { changeDetection: ChangeDetectionStrategy.Default }
      })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MoviesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should list movies in template', () => {
    const moviesMapped: Movie[] = [
      {
        Popularity: 809213,
        VoteCount: 898,
        PosterPath: '/TnOeov4w0sTtV2gqICqIxVi74V.jpg',
        Id: 605116,
        Adult: false,
        BackdropPath: '/qVygtf2vU15L2yKS4Ke44U4oMdD.jpg',
        OriginalLanguage: 'en',
        OriginalTitle: 'Project Power',
        Title: 'Project Power',
        VoteAverage: 6.7,
        Overview: 'An ex-soldier, a teen and a cop collide in New Orleans as they hunt for the source behind a dangerous new pill that grants users temporary superpowers.',
        ReleaseDate: '2020-08-14'
      },
      {
        Popularity: 610345,
        VoteCount: 91,
        PosterPath: '/uOw5JD8IlD546feZ6oxbIjvN66P.jpg',
        Id: 718444,
        Adult: false,
        BackdropPath: '/x4UkhIQuHIJyeeOTdcbZ3t3gBSa.jpg',
        OriginalLanguage: 'en',
        OriginalTitle: 'Rogue',
        Title: 'Rogue',
        VoteAverage: 5.9,
        Overview: 'Battle-hardened O’Hara leads a lively mercenary team of soldiers on a daring mission: rescue hostages from their captors in remote Africa. But as the mission goes awry and the team is stranded, O’Hara’s squad must face a bloody, brutal encounter with a gang of rebels.',
        ReleaseDate: '2020-08-20'
      }
    ];

    component.movies$ = of(moviesMapped);
    component.selectedMovie$ = of(moviesMapped[0]);
    fixture.detectChanges();
    expect(fixture.debugElement.queryAll(By.css('button')).length).toBe(moviesMapped.length);
  });

});
