import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { Store } from '@ngrx/store';
import { MoviesState } from 'src/app/store/movies-store/movies.reducer';
import { Observable } from 'rxjs';
import { Movie } from 'src/app/shared/models/movie';
import * as moviesSelectors from '../../store/movies-store/movies.selectors';
import { environment } from 'src/environments/environment';
import { Utils } from 'src/app/shared/utils/utils';

@Component({
  selector: 'app-movie-details',
  templateUrl: './movie-details.component.html',
  styleUrls: ['./movie-details.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MovieDetailsComponent implements OnInit {
  movie$: Observable<Movie>;
  environment = environment;

  Utils = Utils;

  constructor(private store: Store<MoviesState>) { }

  ngOnInit(): void {
    this.movie$ = this.store.select(moviesSelectors.selectMovie);
  }

}
