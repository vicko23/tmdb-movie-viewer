import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MovieDetailsComponent } from './movie-details.component';
import { RouterModule } from '@angular/router';
import { provideMockStore } from '@ngrx/store/testing';
import { ChangeDetectionStrategy, NO_ERRORS_SCHEMA } from '@angular/core';
import { of } from 'rxjs';
import { Movie } from 'src/app/shared/models/movie';
import { By } from '@angular/platform-browser';

describe('MovieDetailsComponent', () => {
  let component: MovieDetailsComponent;
  let fixture: ComponentFixture<MovieDetailsComponent>;

  const initialState = {
    movies: {
      ids: [],
      entities: [],
      selectedMovie: null,
      statusState: 'INIT'
    }
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [MovieDetailsComponent],
      imports: [
        RouterModule.forRoot([])
      ],
      providers: [
        provideMockStore({ initialState })
      ],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .overrideComponent(MovieDetailsComponent, {
        set: { changeDetection: ChangeDetectionStrategy.Default }
      })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MovieDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should set movie details in template', async(() => {
    const movie: Movie = {
      Popularity: 809213,
      VoteCount: 898,
      PosterPath: '/TnOeov4w0sTtV2gqICqIxVi74V.jpg',
      Id: 605116,
      Adult: false,
      BackdropPath: '/qVygtf2vU15L2yKS4Ke44U4oMdD.jpg',
      OriginalLanguage: 'en',
      OriginalTitle: 'Project Power',
      Title: 'Project Power',
      VoteAverage: 6.7,
      Overview: 'An ex-soldier, a teen and a cop collide in New Orleans as they hunt for the source behind a dangerous new pill that grants users temporary superpowers.',
      ReleaseDate: '2020-08-14'
    };
    component.movie$ = of(movie);
    fixture.detectChanges();
    expect(fixture.debugElement.query(By.css('h1')).nativeElement.innerText).toBe(movie.Title);
    expect(fixture.debugElement.query(By.css('.img-poster')).nativeElement.src).toContain(movie.PosterPath);
    expect(fixture.debugElement.query(By.css('.col-md-12 p')).nativeElement.innerText).toBe(movie.Overview);
    expect(fixture.debugElement.query(By.css('.col-6 h4')).nativeElement.innerText).toContain('14/08/2020');
    expect(fixture.debugElement.queryAll(By.css('h4.rating-heading'))[1].nativeElement.innerText).toContain(movie.VoteAverage);
    expect(fixture.debugElement.query(By.css('.img-thumbnail.img-fluid.mx-auto.d-block')).nativeElement.src).toContain(movie.BackdropPath);
  }));

});
