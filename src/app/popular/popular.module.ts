import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PopularComponent } from './popular.component';
import { SharedModule } from '../shared/shared.module';
import { FaIconLibrary } from '@fortawesome/angular-fontawesome';
import { faCaretRight } from '@fortawesome/free-solid-svg-icons';
import { MovieDetailsComponent } from './movie-details/movie-details.component';
import { MoviesComponent } from './movies/movies.component';

@NgModule({
  declarations: [PopularComponent, MovieDetailsComponent, MoviesComponent],
  imports: [CommonModule, SharedModule],
})
export class PopularModule {
  constructor(library: FaIconLibrary) {
    library.addIcons(faCaretRight);
  }
}
