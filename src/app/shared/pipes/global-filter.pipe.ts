import { Pipe, PipeTransform } from '@angular/core';
@Pipe({
  name: 'globalFilter',
  pure: true
})
export class GlobalFilterPipe implements PipeTransform {
  transform(items: any[], searchText: string): any[] {
    if (!items) {
      return [];
    }
    if (!searchText) {
      return items;
    }
    searchText = searchText.toLowerCase();
    return items.filter((item) => {
      for (const value of Object.values(item)) {
        if (value.toString().toLowerCase().includes(searchText)) {
          return true;
        }
      }
    });
  }
}
