import { GlobalFilterPipe } from './global-filter.pipe';

describe('GlobalFilterPipe', () => {
    const pipe = new GlobalFilterPipe();

    const data = [
        {
            Adult: false,
            BackdropPath: 'some-path',
            Id: 1,
            OriginalLanguage: 'en',
            OriginalTitle: 'Some Movie',
            Overview: 'Some overview',
            Popularity: 50,
            PosterPath: 'some-path',
            ReleaseDate: 'Some Date',
            Title: 'Some Title',
            VoteAverage: 8,
            VoteCount: 500
        },
        {
            Adult: false,
            BackdropPath: 'some-path-2',
            Id: 1,
            OriginalLanguage: 'en',
            OriginalTitle: 'Great movie',
            Overview: 'Some overview',
            Popularity: 50,
            PosterPath: 'some-path',
            ReleaseDate: 'Some Date',
            Title: 'Great Title',
            VoteAverage: 8,
            VoteCount: 500
        }
    ];

    it('should find movie', () => {
        expect(pipe.transform(data, 'great')).toEqual([data[1]]);
    });
});
