export class Movie {
  Popularity: number;
  VoteCount: number;
  PosterPath: string;
  Id: number;
  Adult: boolean;
  BackdropPath: string;
  OriginalLanguage: string;
  OriginalTitle: string;
  Title: string;
  VoteAverage: number;
  Overview: string;
  ReleaseDate: string;
}
