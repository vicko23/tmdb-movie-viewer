export class Utils {
    /**
    * Track by fn
    * used for tracking ngFor changes
    * @param index index of item
    * @param item item in list
    * @returns index
    */
    static trackByFn(index: any, item: any): any {
        return index;
    }

    /**
    * Round number
    * used to round number to closer integer
    * @param value number to round
    * @returns rounded number
    */
    static roundNumber(value: number): number {
        return Math.round(value);
    }
}
