import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  FontAwesomeModule,
  FaIconLibrary,
} from '@fortawesome/angular-fontawesome';
import { FormsModule } from '@angular/forms';
import { GlobalFilterPipe } from './pipes/global-filter.pipe';

import { faSearch, faStar } from '@fortawesome/free-solid-svg-icons';
import { StarRatingComponent } from './components/star-rating/star-rating.component';

@NgModule({
  declarations: [GlobalFilterPipe, StarRatingComponent],
  imports: [CommonModule, FontAwesomeModule, FormsModule],
  exports: [FontAwesomeModule, FormsModule, GlobalFilterPipe, StarRatingComponent],
  providers: [GlobalFilterPipe]
})
export class SharedModule {
  constructor(library: FaIconLibrary) {
    library.addIcons(faSearch, faStar);
  }
}
