import { Component, Input, ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'app-star-rating',
  templateUrl: './star-rating.component.html',
  styleUrls: ['./star-rating.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class StarRatingComponent {
  stars: number[] = [1, 2, 3, 4, 5];
  @Input() selectedValue: number;
}
