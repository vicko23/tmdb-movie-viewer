import { Action, createReducer, on } from '@ngrx/store';
import * as MoviesActions from './movies.actions';
import { Movie } from '../../shared/models/movie';
import { StatusState, LoadingState } from '../shared/status.reducer';
import { EntityState, EntityAdapter, createEntityAdapter } from '@ngrx/entity';

export const moviesFeatureKey = 'movies';

export interface MoviesState extends EntityState<Movie> {
  selectedMovie: Movie;
  statusState: StatusState;
}

export const adapter: EntityAdapter<Movie> = createEntityAdapter<Movie>({
  selectId: (movie: Movie) => movie.Id,
});

export const initialState: MoviesState = adapter.getInitialState({
  // additional entity state properties
  selectedMovie: null,
  statusState: LoadingState.INIT,
});

const moviesReducer = createReducer(
  initialState,
  on(MoviesActions.loadMovies, (state, action) => {
    return {
      ...state,
      statusState: LoadingState.LOADING,
    };
  }),
  on(MoviesActions.loadMoviesSuccess, (state, action) => {
    return adapter.setAll(action.movies, {
      ...state,
      selectedMovie: action.movies[0],
      statusState: LoadingState.LOADED,
    });
  }),
  on(MoviesActions.loadMoviesFailure, (state, action) => {
    return {
      ...state,
      statusState: { error: action.error },
    };
  }),
  on(MoviesActions.selectMovie, (state, action) => {
    return {
      ...state,
      selectedMovie: action.movie,
    };
  })
);

export function reducer(
  state: MoviesState | undefined,
  action: Action
): MoviesState {
  return moviesReducer(state, action);
}

export const { selectIds, selectEntities, selectAll, selectTotal } = adapter.getSelectors();
