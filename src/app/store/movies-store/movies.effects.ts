import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import * as fromMoviesActions from './movies.actions';
import { MoviesService } from '../../core/services/movies.service';
import { of } from 'rxjs';
import { map, mergeMap, catchError } from 'rxjs/operators';

@Injectable()
export class MoviesEffects {
  constructor(private actions$: Actions, private movieService: MoviesService) { }

  loadMovies$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromMoviesActions.loadMovies),
      mergeMap(() =>
        this.movieService.getMovies().pipe(
          map((movies) => fromMoviesActions.loadMoviesSuccess({ movies })),
          // error handling is in interceptor but,
          // it may be possible to have it here for some HTTP request (needs change in interceptor)
          catchError((error) =>
            of(fromMoviesActions.loadMoviesFailure({ error }))
          )
        )
      )
    )
  );
}
