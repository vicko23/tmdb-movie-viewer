import * as selectors from '../movies.selectors';
import { MoviesState } from '../movies.reducer';
import { LoadingState } from '../../shared/status.reducer';

const defaultMovie = {
    Adult: false,
    BackdropPath: 'some-path',
    Id: 1,
    OriginalLanguage: 'en',
    OriginalTitle: 'Some Movie',
    Overview: 'Some overview',
    Popularity: 50,
    PosterPath: 'some-path',
    ReleaseDate: 'Some Date',
    Title: 'Some Title',
    VoteAverage: 8,
    VoteCount: 500
};

const createMovieState = (): MoviesState => ({
    selectedMovie: defaultMovie,
    entities: {
        1: defaultMovie
    },
    statusState: LoadingState.LOADED,
    ids: [1]
});

const createState = (): any => ({
    movies: createMovieState()
});

describe('Store > Movies > Selectors', () => {
    const state = createState();
    it('#selectMoviesStateCheck', () => {
        const movieEntities = [];
        for (const key of Object.keys(state.movies.entities)) {
            const value = state.movies.entities[key];
            movieEntities.push(value);
        }
        expect(selectors.selectMovies(state)).toEqual(movieEntities);
        expect(selectors.selectMovie(state)).toEqual(state.movies.selectedMovie);
    });
});
