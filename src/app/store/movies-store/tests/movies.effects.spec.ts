import { Observable, of } from 'rxjs';
import { Action } from '@ngrx/store';
import { TestBed, async } from '@angular/core/testing';
import { provideMockActions } from '@ngrx/effects/testing';
import { provideMockStore } from '@ngrx/store/testing';

import { MoviesEffects } from '../movies.effects';
import * as fromActions from '../movies.actions';
import { MoviesService } from '../../../core/services/movies.service';
import { Movie } from 'src/app/shared/models/movie';

describe('Store > Movies > Effects', () => {
  let actions$: Observable<Action>;
  let effects: MoviesEffects;
  let moviesService: any;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      providers: [
        MoviesEffects,
        provideMockStore(),
        provideMockActions(() => actions$),
        {
          provide: MoviesService,
          useValue: jasmine.createSpyObj('MoviesService', ['getMovies'])
        }
      ],
    });

    effects = TestBed.inject<MoviesEffects>(MoviesEffects);
    moviesService = TestBed.inject<MoviesService>(MoviesService);
  }));

  it('SHOULD dispatch Load Movies Success action WHEN Load Movies Request action is dispatched', () => {
    const movies: Movie[] = [
      {
        Adult: false,
        BackdropPath: 'some-path',
        Id: 1,
        OriginalLanguage: 'en',
        OriginalTitle: 'Some Movie',
        Overview: 'Some overview',
        Popularity: 50,
        PosterPath: 'some-path',
        ReleaseDate: 'Some Date',
        Title: 'Some Title',
        VoteAverage: 8,
        VoteCount: 500
      }
    ];
    moviesService.getMovies.and.returnValue(of(movies));

    actions$ = of({ type: fromActions.loadMovies.type });

    effects.loadMovies$.subscribe(action => {
      expect(action.type).toBe(fromActions.loadMoviesSuccess.type);
      expect((action as any).movies).toBe(movies);
    });
  });
});
