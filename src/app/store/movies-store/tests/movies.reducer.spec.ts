import * as fromReducer from '../movies.reducer';
import * as fromActions from '../movies.actions';
import { Movie } from 'src/app/shared/models/movie';
import { StatusState, LoadingState } from '../../shared/status.reducer';

describe('Store > Movies > Reducer', () => {
    let defaultMovie: Movie;

    beforeEach(() => {
        defaultMovie = {
            Adult: false,
            BackdropPath: 'some-path',
            Id: 1,
            OriginalLanguage: 'en',
            OriginalTitle: 'Some Movie',
            Overview: 'Some overview',
            Popularity: 50,
            PosterPath: 'some-path',
            ReleaseDate: 'Some Date',
            Title: 'Some Title',
            VoteAverage: 8,
            VoteCount: 500
        };
    });

    afterEach(() => {
        fromReducer.initialState.entities = {};
        fromReducer.initialState.ids = [];
        fromReducer.initialState.selectedMovie = null;
        fromReducer.initialState.statusState = null;
    });

    it('SHOULD return the default state', () => {
        const { initialState } = fromReducer;
        const state = fromReducer.reducer(undefined, { type: null });

        expect(state).toBe(initialState);
    });

    it('SHOULD request movies', () => {
        const { initialState } = fromReducer;
        const payload: StatusState = LoadingState.LOADING;
        const action = fromActions.loadMovies;
        const state = fromReducer.reducer(initialState, action);

        expect(state.statusState).toEqual(payload);
    });

    it('SHOULD set movies', () => {
        const { initialState } = fromReducer;
        const movies: Movie[] = [defaultMovie];
        const payload = {
            1: movies[0]
        };
        const action = fromActions.loadMoviesSuccess({ movies });
        const state = fromReducer.reducer(initialState, action);

        expect(state.entities).toEqual(payload);
        expect(state.selectedMovie).toEqual(defaultMovie);
    });

    it('SHOULD set error', () => {
        const { initialState } = fromReducer;
        const error = new Error('test error');
        const payload = {
            error
        };
        const action = fromActions.loadMoviesFailure({ error });
        const state = fromReducer.reducer(initialState, action);

        expect(state.statusState).toEqual(payload);
    });

    it('SHOULD select movie', () => {
        const { initialState } = fromReducer;
        const action = fromActions.selectMovie({ movie: defaultMovie });
        const state = fromReducer.reducer(initialState, action);

        expect(state.selectedMovie).toEqual(defaultMovie);
    });
});
