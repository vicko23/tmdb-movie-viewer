import * as Actions from '../movies.actions';
import { Movie } from 'src/app/shared/models/movie';

describe('Store > Movies > Actions', () => {

  it('should create a [Movie Page] Load Movies Request action', () => {
    const action = Actions.loadMovies;
    expect(action.type).toEqual(Actions.loadMovies.type);
  });

  it('should create a [Movie API] Load Movies Success action containing a payload', () => {
    const payload: Movie[] = [];
    const action = Actions.loadMoviesSuccess({ movies: payload });

    expect({ ...action }).toEqual({
      type: Actions.loadMoviesSuccess.type,
      movies: payload
    });
  });

  it('should create a [Movie API] Load Movies Failure action containing a payload', () => {
    const payload: Error = new Error();
    const action = Actions.loadMoviesFailure({ error: payload });

    expect({ ...action }).toEqual({
      type: Actions.loadMoviesFailure.type,
      error: payload
    });
  });

  it('should create a [Movie Page] Select Movie action containing a payload', () => {
    const payload: Movie = new Movie();
    const action = Actions.selectMovie({ movie: payload });

    expect({ ...action }).toEqual({
      type: Actions.selectMovie.type,
      movie: payload
    });
  });
});
