import { createAction, props } from '@ngrx/store';
import { Movie } from '../../shared/models/movie';

export const loadMovies = createAction('[Movie Page] Load Movies Request');

export const loadMoviesSuccess = createAction(
  '[Movie API] Load Movies Success',
  props<{ movies: Movie[] }>()
);
export const loadMoviesFailure = createAction(
  '[Movie API] Load Movies Failure',
  props<{ error: any }>()
);

export const selectMovie = createAction(
  '[Movie Page] Select Movie',
  props<{ movie: Movie }>()
);
