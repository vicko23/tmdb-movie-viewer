import { createFeatureSelector, createSelector } from '@ngrx/store';
import { MoviesState, moviesFeatureKey, selectAll } from './movies.reducer';
import { LoadingState, getError } from '../shared/status.reducer';

export const selectMoviesState = createFeatureSelector<MoviesState>(
  moviesFeatureKey
);

export const selectMovies = createSelector(selectMoviesState, selectAll);

export const selectMovie = createSelector(
  selectMoviesState,
  (state: MoviesState) => state.selectedMovie
);

export const selectMoviesLoading = createSelector(
  selectMoviesState,
  (state: MoviesState) => state.statusState === LoadingState.LOADING
);

export const selectMoviesError = createSelector(
  selectMoviesState,
  (state: MoviesState) => getError(state.statusState)
);
